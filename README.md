# README #

This repository contains an example for a LabView virtual instrument to acquire data from an LPMS-B2. 
It is a simple example and can be easily customized to work with other LPMS units.
A blog pots on the topic is here: https://lp-research.com/lpms-and-labview/